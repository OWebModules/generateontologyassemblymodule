﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateOntologyAssembly.Models
{
    public class GenerateAssemblyRequest
    {
        public string IdOntologyUpdate { get; private set; }
        public string NewVersion { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }
        
        public GenerateAssemblyRequest(string idOntologyUpdate, string newVersion = null)
        {
            IdOntologyUpdate = idOntologyUpdate;
            NewVersion = newVersion;
        }
    }
}
