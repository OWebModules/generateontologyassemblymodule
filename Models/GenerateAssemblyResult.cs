﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateOntologyAssembly.Models
{
    public class GenerateAssemblyResult
    {
        public clsOntologyItem ResultState { get; set; }

    }
}
