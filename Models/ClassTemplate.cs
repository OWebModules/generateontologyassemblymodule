﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateOntologyAssembly.Models
{
    public class ClassTemplate
    {
        private string templateClass;

        public string Name { get; set; }
        public StringBuilder SbAttributes { get; private set; } = new StringBuilder();

        public string GetClassCode()
        {
            var template = templateClass.Replace("@CLASS_NAME@", Name);
            template = template.Replace("@PROPERTY_LIST@", SbAttributes.ToString());

            return template;

        }

        

        public ClassTemplate(string templateClass)
        {
            this.templateClass = templateClass;

        }
    }

}
