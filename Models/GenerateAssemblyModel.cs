﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateOntologyAssembly.Models
{
    public class GenerateAssemblyModel
    {
        public clsOntologyItem OntologyUpdate { get; set; }
        public clsObjectAtt Build { get; set; }
        public clsObjectAtt Major { get; set; }
        public clsObjectAtt Minor { get; set; }
        public clsObjectAtt Revision { get; set; }
        public clsObjectRel Namespace { get; set; }
        public clsObjectRel Ontology { get; set; }
        public clsObjectRel Path { get; set; }
    }
}
