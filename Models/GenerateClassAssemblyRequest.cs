﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateOntologyAssembly.Models
{
    public class GenerateClassAssemblyRequest
    {
        
        public string IdOntology { get; private set; }
        public string IdClassRoot { get; private set; }
        public string Version { get; private set; }
        public string Namespace { get; private set; }
        public string FileName { get; private set; }

        public string DirectoryPath { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public GenerateClassAssemblyRequest(string idOntology, string version, string nameSpace, string fileName, string directoryPath, string idClassRoot)
        {
            IdOntology = idOntology;
            IdClassRoot = idClassRoot;
            Version = version;
            Namespace = nameSpace;
            FileName = fileName;
            DirectoryPath = directoryPath;
        }
    }
}
