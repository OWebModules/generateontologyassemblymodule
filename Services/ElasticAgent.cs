﻿using GenerateOntologyAssembly.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateOntologyAssembly.Services
{
    public class ElasticAgent
    {
        private Globals globals;

        public async Task<ResultItem<GenerateAssemblyModel>> GetGenerateAssemblyModel(GenerateAssemblyRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GenerateAssemblyModel>>(() =>
           {
               var result = new ResultItem<GenerateAssemblyModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GenerateAssemblyModel()
               };

               var searchAttributes = new List<clsObjectAtt>
               {
                   new clsObjectAtt
                   {
                       ID_Object = request.IdOntologyUpdate,
                       ID_AttributeType =  /*"e364d837b5d84c04b951e0bd1631334d"*/Config.LocalData.ClassAtt_Ontology_Update_Build.ID_AttributeType
                   },
                   new clsObjectAtt
                   {
                       ID_Object = request.IdOntologyUpdate,
                       ID_AttributeType =  /*"e57ea6f477d54cc79070d0ea3308092f"*/Config.LocalData.ClassAtt_Ontology_Update_Major.ID_AttributeType
                   },
                   new clsObjectAtt
                   {
                       ID_Object = request.IdOntologyUpdate,
                       ID_AttributeType = /*"6c72b7e7a18241359c4e63c8885b07b8"*/Config.LocalData.ClassAtt_Ontology_Update_Minor.ID_AttributeType
                   },
                   new clsObjectAtt
                   {
                       ID_Object = request.IdOntologyUpdate,
                       ID_AttributeType = /*"d4c755c14ebe4c1c9be189244e1bf8d3"*/Config.LocalData.ClassAtt_Ontology_Update_Revision.ID_AttributeType
                   }
               };

               var dbReaderAttributes = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Attributes!";
                   return result;
               }

               result.Result.Major = dbReaderAttributes.ObjAtts.FirstOrDefault(att => att.ID_AttributeType == /*"e57ea6f477d54cc79070d0ea3308092f"*/ Config.LocalData.ClassAtt_Ontology_Update_Major.ID_AttributeType);
               result.Result.Minor = dbReaderAttributes.ObjAtts.FirstOrDefault(att => att.ID_AttributeType == /*"6c72b7e7a18241359c4e63c8885b07b8"*/ Config.LocalData.ClassAtt_Ontology_Update_Minor.ID_AttributeType);
               result.Result.Build = dbReaderAttributes.ObjAtts.FirstOrDefault(att => att.ID_AttributeType == /*"e364d837b5d84c04b951e0bd1631334d"*/ Config.LocalData.ClassAtt_Ontology_Update_Build.ID_AttributeType);
               result.Result.Revision = dbReaderAttributes.ObjAtts.FirstOrDefault(att => att.ID_AttributeType == /*"d4c755c14ebe4c1c9be189244e1bf8d3"*/ Config.LocalData.ClassAtt_Ontology_Update_Revision.ID_AttributeType);

               if (result.Result.Major == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to add a Major-number";
                   return result;
               }

               if (result.Result.Minor == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to add a Minor-number";
                   return result;
               }

               if (result.Result.Build == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to add a Build-number";
                   return result;
               }

               if (result.Result.Revision == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to add a Revision-number";
                   return result;
               }

               var searchRelations = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = request.IdOntologyUpdate,
                       ID_RelationType = /*"e07469d9766c443e85266d9c684f944f",*/ Config.LocalData.ClassRel_Ontology_Update_belongs_to_Ontologies.ID_RelationType,
                       ID_Parent_Other = /*"eb411e2ff93d4a5ebbbac0b5d7ec0197"*/ Config.LocalData.ClassRel_Ontology_Update_belongs_to_Ontologies.ID_Class_Right
                   },
                   new clsObjectRel
                   {
                       ID_Object = request.IdOntologyUpdate,
                       ID_RelationType = /*"e07469d9766c443e85266d9c684f944f",*/ Config.LocalData.ClassRel_Ontology_Update_belongs_to_Namespace.ID_RelationType,
                       ID_Parent_Other = /*"2a5a444c07534cb1ae6bfea1b5c747be"*/ Config.LocalData.ClassRel_Ontology_Update_belongs_to_Namespace.ID_Class_Right
                   },
                   new clsObjectRel
                   {
                       ID_Object = request.IdOntologyUpdate,
                       ID_RelationType = /*"d34d545e9ddf46cebb6f22db1b7bb025",*/ Config.LocalData.ClassRel_Ontology_Update_belonging_Source_Path.ID_RelationType,
                       ID_Parent_Other = /*"8a894710e08c42c5b829ef4809830d33"*/ Config.LocalData.ClassRel_Ontology_Update_belonging_Source_Path.ID_Class_Right
                   }
               };

               var dbReaderRelations = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelations);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relations!";
                   return result;
               }

               result.Result.Namespace = dbReaderRelations.ObjectRels.FirstOrDefault(rel => rel.ID_Parent_Other == /*"2a5a444c07534cb1ae6bfea1b5c747be"*/ Config.LocalData.ClassRel_Ontology_Update_belongs_to_Namespace.ID_Class_Right);
               result.Result.Ontology = dbReaderRelations.ObjectRels.FirstOrDefault(rel => rel.ID_Parent_Other == /* "eb411e2ff93d4a5ebbbac0b5d7ec0197" */ Config.LocalData.ClassRel_Ontology_Update_belongs_to_Ontologies.ID_Class_Right);
               result.Result.Path = dbReaderRelations.ObjectRels.FirstOrDefault(rel => rel.ID_Parent_Other == /*"8a894710e08c42c5b829ef4809830d33" */ Config.LocalData.ClassRel_Ontology_Update_belonging_Source_Path.ID_Class_Right );

               if (result.Result.Namespace == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to add a Namespace";
                   return result;
               }

               if (result.Result.Ontology    == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to add an Ontology";
                   return result;
               }

               if (result.Result.Path == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "You have to add a Path";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public ElasticAgent(Globals globals)
        {
            this.globals = globals;


        }
    }
}
