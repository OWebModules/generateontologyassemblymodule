﻿using GenerateOntologyAssembly.Models;
using GenerateOntologyAssembly.Primitives;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Services;
using OntoMsg_Module.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GenerateOntologyAssembly.Services;

namespace GenerateOntologyAssembly
{
    public class GenerateOntologyAssemblyController : AppController
    {

        private Dictionary<string, string> ReplaceTokens = new Dictionary<string, string>();

        private string GetPropertyName(string name, Regex regex)
        {
            var result = "";
            if (ReplaceTokens.ContainsKey(name))
            {
                result = ReplaceTokens[name];
            }
            else
            {
                foreach (var item in name)
                {

                    if (!regex.Match(item.ToString()).Success)
                    {
                        result += "_";
                    }
                    else
                    {
                        result += item;
                    }
                }
            }
            
            return result;
        }

        private string GetUniqueName(string name, string type, List<string> names, string idParent)
        {
            if (idParent == Globals.Class_OntologyItems.GUID)
            {
                type = $"{type}_OItem";
            }

            var newName = $"{type}_{name}";
            var i = 1;

            while (names.Any(oItm => oItm == newName))
            {
                newName = $"{type}_{name}{i}";
                i++;
            }

            return newName;
        }
        public async Task<ResultItem<GenerateClassAssemblyResult>> GenerateClassAssembly(GenerateClassAssemblyRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GenerateClassAssemblyResult>>(async () =>
            {
                var result = new ResultItem<GenerateClassAssemblyResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GenerateClassAssemblyResult()
                };

                var ontologyController = new OntologyConnector(Globals);

                var controllerResult = await ontologyController.GetOntologies(new GetOntologyRequest(new clsOntologyItem
                {
                    GUID = request.IdOntology,
                    GUID_Parent = ontologyController.Globals.Class_Ontologies.GUID,
                    Type = Globals.Type_Object
                }));

                result.ResultState = controllerResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting ontologies!";
                    return result;
                }

                var enrichResult = await ontologyController.EnrichOntologies(controllerResult.Result);
                result.ResultState = enrichResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while enrich ontologies!";
                    return result;
                }

                try
                {

                    Version version = new Version(request.Version);
                    var parameters = new CompilerParameters();
                    parameters.ReferencedAssemblies.Add("OntologyClasses.dll");

                    parameters.GenerateExecutable = false;
                    parameters.OutputAssembly = Path.Combine(request.DirectoryPath, request.FileName);

                    var sbOntologyAssembly = new StringBuilder();

                    sbOntologyAssembly.Append(Templates.ClassTemplateClassOntologyRoot);
                    sbOntologyAssembly.Replace("@VERSION_NUMBER@", request.Version);
                    sbOntologyAssembly.Replace("@NAMESPACE@", request.Namespace);
                    var sbOntologyAssemblyProperties = new StringBuilder();

                    var sbSimpleProperties = new StringBuilder();
                    

                    var classDefinitionResult = GetClassDefinition(request.IdClassRoot,
                        enrichResult.Result.SelectMany(ont => ont.ClassAttributes),
                        enrichResult.Result.SelectMany(ont => ont.ClassRelations),
                        enrichResult.Result.SelectMany(ont => ont.Classes),
                        enrichResult.Result.SelectMany(ont => ont.AttributeTypes),
                        enrichResult.Result.SelectMany(ont => ont.RelationTypes),
                        new List<ClassTemplate>());

                    result.ResultState = classDefinitionResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var classTemplate = sbOntologyAssembly.Replace("@CLASS_DEFINITIONS@", string.Join("\n", classDefinitionResult.Result.Select(clsDef => clsDef.GetClassCode()))).ToString();

                    CodeDomProvider objCodeCompiler = new Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider();
                    CompilerResults r = objCodeCompiler.CompileAssemblyFromSource(parameters, classTemplate);

                    if (r.Errors.HasErrors)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = string.Join("\r\n", r.Errors.Cast<CompilerError>().Select(err => err.ErrorText));
                        return result;
                    }


                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Generate: {ex.Message}";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ResultItem<List<ClassTemplate>> GetClassDefinition(string idClass, IEnumerable<clsClassAtt> classAttributes,
            IEnumerable<clsClassRel> classRelations,
            IEnumerable<clsOntologyItem> classes,
            IEnumerable<clsOntologyItem> attributeTypes,
            IEnumerable<clsOntologyItem> relationTypes,
            List<ClassTemplate> classTemplates)
        {
            var result = new ResultItem<List<ClassTemplate>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ClassTemplate>()
            };

            var regex = new Regex(@"[A-Za-z_0-9]");

            var classItem = classes.FirstOrDefault(cls => cls.GUID == idClass);
            

            var classTemplate = new ClassTemplate(Templates.ClassDefinitionTemplate);
            classTemplate.Name = GetPropertyName(classItem.Name,regex);
            result.Result.Add(classTemplate);
            
            foreach (var classAtt in (from clsAtt in classAttributes.Where(clsAtt => clsAtt.ID_Class == idClass)
                                      join cls in classes on clsAtt.ID_Class equals cls.GUID
                                      join attType in attributeTypes on clsAtt.ID_AttributeType equals attType.GUID
                                      select new { clsAtt, cls, attType }))
            {
                
                var dataTypeAttribute = "";
                var propertyTemplate = "";
                if (classAtt.clsAtt.ID_DataType == Globals.DType_Bool.GUID)
                {
                    var dataType = "Bool";
                    if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 0)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}AttributeNullable");
                        
                    }
                    else if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 1)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                    else
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeListProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                    

                }
                else if (classAtt.clsAtt.ID_DataType == Globals.DType_DateTime.GUID)
                {
                    var dataType = "DateTime";
                    if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 0)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}AttributeNullable");
                        
                    }
                    else if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 1)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", dataTypeAttribute = $"{dataType}Attribute");
                    }
                    else
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeListProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                }
                else if (classAtt.clsAtt.ID_DataType == Globals.DType_Int.GUID)
                {
                    var dataType = "Long";
                    if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 0)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}AttributeNullable");
                    }
                    else if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 1)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                    else if (classAtt.clsAtt.Min != 0)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeListProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                }
                else if (classAtt.clsAtt.ID_DataType == Globals.DType_Real.GUID)
                {
                    var dataType = "Double";
                    if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 0)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}AttributeNullable");
                        
                    }
                    else if (classAtt.clsAtt.Max == 1 && classAtt.clsAtt.Min == 1)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                    else if (classAtt.clsAtt.Min != 0)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeListProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                }
                else if (classAtt.clsAtt.ID_DataType == Globals.DType_String.GUID)
                {
                    var dataType = "String";
                    if (classAtt.clsAtt.Max != 1)
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                    else
                    {
                        propertyTemplate = Templates.ClassTemplateAttributeListProp.Replace("@DATA_TYPE_ATTRIBUTE@", $"{dataType}Attribute");
                    }
                }

                var name = GetPropertyName(classAtt.attType.Name, regex);

                propertyTemplate = propertyTemplate.Replace("@NAME_PROP@", name);

                classTemplate.SbAttributes.AppendLine(propertyTemplate);
            }

            foreach (var classRel in (from clsRel in classRelations.Where(classRel => classRel.ID_Class_Left == idClass)
                                      join clsRight in classes on clsRel.ID_Class_Right equals clsRight.GUID
                                      join relType in relationTypes on clsRel.ID_RelationType equals relType.GUID
                                      select new { ID_Class_Left = clsRel.ID_Class_Left,
                                          ID_Class_Right = clsRel.ID_Class_Right,
                                          ID_RelationType = clsRel.ID_RelationType,
                                          Name_Class_Right = clsRight.Name,
                                          Name_RelationType = relType.Name,
                                          Min_Forw = clsRel.Min_Forw,
                                          Max_Forw = clsRel.Max_Forw }).GroupBy(rel => new
                                          {
                                              rel.ID_Class_Left,
                                              rel.ID_Class_Right,
                                              rel.ID_RelationType,
                                              rel.Name_Class_Right,
                                              rel.Name_RelationType,
                                              rel.Min_Forw,
                                              rel.Max_Forw
                                          }).Select(rel => rel.Key))
            {

                var name = "";
                var subClassName = "";
                subClassName = GetPropertyName(classRel.Name_Class_Right, regex);

                if (classRel.Max_Forw == 1)
                {
                    
                    name = subClassName + "Entity";
                } else
                {
                    name = subClassName + "List";
                }
                    

                if (!classRelations.Any(rel => rel.ID_Class_Left == classRel.ID_Class_Right))
                {
                    var relTemplate = Templates.ClassTemplateAttributeRel.Replace("@NAME_CLASS@", "RelationObject");
                    relTemplate = relTemplate.Replace("@NAME_PROP@", name);
                    classTemplate.SbAttributes.AppendLine(relTemplate);
                }
                else
                {
                    var relTemplate = "";

                    if (classRel.Max_Forw == 1)
                    {
                        relTemplate = Templates.ClassTemplateAttributeRel.Replace("@NAME_PROP@", name).Replace("@NAME_CLASS@", subClassName);
                    }
                    else
                    {
                        relTemplate = Templates.ClassTemplateAttributeRelList.Replace("@NAME_PROP@", name).Replace("@NAME_CLASS@", subClassName);
                    }
                    
                    classTemplate.SbAttributes.AppendLine(relTemplate);

                    if (!result.Result.Any(clsRel => clsRel.Name == subClassName) && !classTemplates.Any(clsTemp => clsTemp.Name == subClassName))
                    {
                        var subClassResult = GetClassDefinition(classRel.ID_Class_Right,
                        classAttributes,
                        classRelations,
                        classes,
                        attributeTypes,
                        relationTypes,
                        result.Result);

                        result.ResultState = subClassResult.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result.AddRange(subClassResult.Result);
                    }
                    
                }
            }

            return result;
        }

        public async Task<GenerateAssemblyResult> GenerateAssembly(GenerateAssemblyRequest request)
        {
            var taskResult = await Task.Run<GenerateAssemblyResult>(async () =>
            {
                var result = new GenerateAssemblyResult
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var elasticAgent = new ElasticAgent(Globals);

                request.MessageOutput?.OutputInfo("Get Model...");
                var modelResult = await elasticAgent.GetGenerateAssemblyModel(request);
                request.MessageOutput?.OutputInfo("Have Model");

                result.ResultState = modelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var ontologyController = new OntologyConnector(Globals);

                request.MessageOutput?.OutputInfo("Get Ontologies...");
                var controllerResult = await ontologyController.GetOntologies(new GetOntologyRequest(new clsOntologyItem
                {
                    GUID = modelResult.Result.Ontology.ID_Other,
                    GUID_Parent = ontologyController.Globals.Class_Ontologies.GUID,
                    Type = Globals.Type_Object
                }));

                result.ResultState = controllerResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting ontologies!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Found {controllerResult.Result.Count} Ontologies");

                request.MessageOutput?.OutputInfo($"Enrich Ontologies with Relations...");
                var enrichResult = await ontologyController.EnrichOntologies(controllerResult.Result);
                result.ResultState = enrichResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while enrich ontologies!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Enriched Ontologies with Relations");

                try
                {
                    var versionString = $"{modelResult.Result.Major.Val_Lng}.{modelResult.Result.Minor.Val_Lng}.{modelResult.Result.Build.Val_Lng}.{modelResult.Result.Revision.Val_Lng}";
                    request.MessageOutput?.OutputInfo($"Current version: {versionString}");
                    if (!string.IsNullOrEmpty(request.NewVersion))
                    {
                        versionString = request.NewVersion;
                    }
                    request.MessageOutput?.OutputInfo($"New version: {versionString}");

                    Version version = new Version(versionString);
                    var parameters = new CompilerParameters();
                    parameters.ReferencedAssemblies.Add("OntologyClasses.dll");

                    var path = Path.GetDirectoryName(modelResult.Result.Path.Name_Other);
                    var fileName = Path.GetFileName(modelResult.Result.Path.Name_Other);

                    request.MessageOutput?.OutputInfo($"Path: {path}");
                    request.MessageOutput?.OutputInfo($"Filename: {fileName}");

                    parameters.GenerateExecutable = false;
                    parameters.OutputAssembly = Path.Combine(path, fileName);

                    var listString = Templates.ListsTemplate;
                    var sbOntologyAssembly = new StringBuilder();

                    request.MessageOutput?.OutputInfo($"Add Attributes to Assembly");
                    sbOntologyAssembly.Append(Templates.ClassTemplate);
                    sbOntologyAssembly.Replace("@VERSION_NUMBER@", versionString);
                    sbOntologyAssembly.Replace("@NAMESPACE@", modelResult.Result.Namespace.Name_Other);
                    sbOntologyAssembly.Replace("@ID_ONTOLOGY@", modelResult.Result.Ontology.ID_Other);
                    var sbOntologyAssemblyProperties = new StringBuilder();


                    var attributeTypes = enrichResult.Result.SelectMany(onto => onto.AttributeTypes.GroupBy(itm => new { itm.GUID, itm.Name, itm.GUID_Parent, itm.Type }).Select(itm => new clsOntologyItem { GUID = itm.Key.GUID, Name = itm.Key.Name, GUID_Parent = itm.Key.GUID_Parent, Type = itm.Key.Type })).ToList();
                    var relationTypes = enrichResult.Result.SelectMany(onto => onto.RelationTypes.GroupBy(itm => new { itm.GUID, itm.Name, itm.Type }).Select(itm => new clsOntologyItem { GUID = itm.Key.GUID, Name = itm.Key.Name, Type = itm.Key.Type })).ToList();
                    var classes = enrichResult.Result.SelectMany(onto => onto.Classes.GroupBy(itm => new { itm.GUID, itm.Name, itm.GUID_Parent, itm.Type }).Select(itm => new clsOntologyItem { GUID = itm.Key.GUID, Name = itm.Key.Name, GUID_Parent = itm.Key.GUID_Parent, Type = itm.Key.Type })).ToList();
                    var objects = enrichResult.Result.SelectMany(onto => onto.Objects.GroupBy(itm => new { itm.GUID, itm.Name, itm.GUID_Parent, itm.Type }).Select(itm => new clsOntologyItem { GUID = itm.Key.GUID, Name = itm.Key.Name, GUID_Parent = itm.Key.GUID_Parent, Type = itm.Key.Type })).ToList();

                    request.MessageOutput?.OutputInfo($"{attributeTypes.Count} AttributeTypes; {relationTypes.Count} RelationTypes; {classes.Count} Classes; {objects.Count} Objects");

                    var oItems = attributeTypes;
                    oItems.AddRange(relationTypes);
                    oItems.AddRange(classes);
                    oItems.AddRange(objects);

                    var regex = new Regex(@"[A-Za-z_0-9]");

                    var objectList = new List<string>();
                    var classList = new List<string>();
                    var relationTypeList = new List<string>();
                    var attributeTypeList = new List<string>();

                    var namesToAdd = new List<string>();

                    request.MessageOutput?.OutputInfo($"Add AttributeTypes, RelationTypes, Classes and Objects to Assembly");
                    foreach (clsOntologyItem oItem in oItems.Where(oItem => !string.IsNullOrEmpty(oItem.Name)).Distinct())
                    {

                        var name = GetPropertyName(oItem.Name, regex);
                        var newName = GetUniqueName(name, oItem.Type, namesToAdd, oItem.GUID_Parent);

                        namesToAdd.Add(newName);

                        if (oItem.Type == Globals.Type_Class)
                        {
                            classList.Add(newName);
                        } else if (oItem.Type == Globals.Type_Object)
                        {
                            objectList.Add(newName);
                        } else if (oItem.Type == Globals.Type_AttributeType)
                        {
                            attributeTypeList.Add(newName);
                        } else if (oItem.Type == Globals.Type_RelationType)
                        {
                            relationTypeList.Add(newName);
                        }

                        var template = Templates.OItemProperty.Replace("@NAME_PROP@", newName);
                        template = template.Replace("@GUID@", oItem.GUID);
                        template = template.Replace("@NAME@", oItem.Name.Replace("\\", "\\\\").Replace("\"", "\\\"").Replace("\n",""));
                        template = template.Replace("@GUID_PARENT@", oItem.GUID_Parent);
                        template = template.Replace("@TYPE@", oItem.Type);
                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    var replace = "new List<clsOntologyItem>";
                    if (objectList.Any())
                    {
                        replace += " { " + string.Join(", ", objectList) + " }";
                    }
                    else
                    {
                        replace += "()";
                    }
                    listString = listString.Replace("@OBJECT_LIST@", replace);

                    replace = "new List<clsOntologyItem>";
                    if (classList.Any())
                    {
                        replace += " { " + string.Join(", ", classList) + " }";
                    }
                    else
                    {
                        replace += "()";
                    }
                    listString = listString.Replace("@CLASS_LIST@", replace);

                    replace = "new List<clsOntologyItem>";
                    if (relationTypeList.Any())
                    {
                        replace += " { " + string.Join(", ", relationTypeList) + " }";
                    }
                    else
                    {
                        replace += "()";
                    }
                    listString = listString.Replace("@RELATIONTYPE_LIST@", replace);


                    replace = "new List<clsOntologyItem>";
                    if (attributeTypeList.Any())
                    {
                        replace += " { " + string.Join(", ", attributeTypeList) + " }";
                    }
                    else
                    {
                        replace += "()";
                    }
                    listString = listString.Replace("@ATTRIBUTETYPE_LIST@", replace);

                    namesToAdd.Clear();


                    request.MessageOutput?.OutputInfo($"Add ClassAtts to Assembly");
                    var classAttItems = from classAtt in enrichResult.Result.SelectMany(onto => onto.ClassAttributes)
                                        join classItm in classes on classAtt.ID_Class equals classItm.GUID
                                        join attType in attributeTypes on classAtt.ID_AttributeType equals attType.GUID
                                        select new { classAtt, classItm, attType };
                    foreach (var item in classAttItems)
                    {
                        var className = GetPropertyName(item.classItm.Name, regex);
                        var attributeTypeName = GetPropertyName(item.attType.Name, regex);

                        var name = $"{className}_{attributeTypeName}";
                        var newName = GetUniqueName(name, "ClassAtt", namesToAdd, null);

                        namesToAdd.Add(newName);

                        var template = Templates.ClassAttProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_CLASS@", item.classAtt.ID_Class);
                        template = template.Replace("@ID_ATTRIBUTE_TYPE@", item.classAtt.ID_AttributeType);
                        template = template.Replace("@ID_DATATYPE@", item.classAtt.ID_DataType);
                        template = template.Replace("@MIN@", item.classAtt.Min.ToString());
                        template = template.Replace("@MAX@", item.classAtt.Max.ToString());

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    if (namesToAdd.Any())
                    {
                        listString = listString.Replace("@CLASSATT_LIST@", "new List<clsClassAtt> { " + string.Join(", ", namesToAdd) + " }");
                    }
                    else
                    {
                        listString = listString.Replace("@CLASSATT_LIST@", "new List<clsClassAtt> ()");
                    }

                    namesToAdd.Clear();

                    var classRelList = new List<string>();
                    request.MessageOutput?.OutputInfo($"Add ClassRels (Full) to Assembly");
                    var classesFull = from classItm in enrichResult.Result.SelectMany(onto => onto.ClassRelations.Where(cls => !string.IsNullOrEmpty(cls.ID_Class_Right)))
                                      join leftClass in classes on classItm.ID_Class_Left equals leftClass.GUID
                                      join rightClass in classes on classItm.ID_Class_Right equals rightClass.GUID
                                      join relationTyp in relationTypes on classItm.ID_RelationType equals relationTyp.GUID
                                      select new { classItm, leftClass, rightClass, relationTyp };

                    foreach (var item in classesFull)
                    {
                        var classLeftName = GetPropertyName(item.leftClass.Name, regex);
                        var classLeftRight = GetPropertyName(item.rightClass.Name, regex);
                        var relationTypeName = GetPropertyName(item.relationTyp.Name, regex);

                        var name = $"{classLeftName}_{relationTypeName}_{classLeftRight}";
                        var newName = GetUniqueName(name, "ClassRel", namesToAdd, null);

                        namesToAdd.Add(newName);
                        classRelList.Add(newName);

                        var template = Templates.ClassRelFullProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_CLASS_LEFT@", item.classItm.ID_Class_Left);
                        template = template.Replace("@ID_CLASS_RIGHT@", item.classItm.ID_Class_Right);
                        template = template.Replace("@ID_RELATIONTYPE@", item.classItm.ID_RelationType);
                        template = template.Replace("@ONTOLOGY@", item.classItm.Ontology);
                        template = template.Replace("@MIN_FORW@", item.classItm.Min_Forw.ToString());
                        template = template.Replace("@MAX_FORW@", item.classItm.Max_Forw.ToString());
                        template = template.Replace("@MAX_BACKW@", item.classItm.Max_Backw.ToString());

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    namesToAdd.Clear();

                    request.MessageOutput?.OutputInfo($"Add ClassRels (Light) to Assembly");
                    var classesPart = from classItm in enrichResult.Result.SelectMany(onto => onto.ClassRelations.Where(cls => string.IsNullOrEmpty(cls.ID_Class_Right)))
                                      join leftClass in classes on classItm.ID_Class_Left equals leftClass.GUID
                                      join relationTyp in relationTypes on classItm.ID_RelationType equals relationTyp.GUID
                                      select new { classItm, leftClass, relationTyp };

                    foreach (var item in classesPart)
                    {
                        var classLeftName = GetPropertyName(item.leftClass.Name, regex);
                        var relationTypeName = GetPropertyName(item.relationTyp.Name, regex);

                        var name = $"{classLeftName}_{relationTypeName}";
                        var newName = GetUniqueName(name, "ClassRel", namesToAdd, null);

                        namesToAdd.Add(newName);
                        classRelList.Add(newName);

                        var template = Templates.ClassRelFullProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_CLASS_LEFT@", item.classItm.ID_Class_Left);
                        template = template.Replace("@ID_RELATIONTYPE@", item.classItm.ID_RelationType);
                        template = template.Replace("@ONTOLOGY@", item.classItm.Ontology);
                        template = template.Replace("@MIN_FORW@", item.classItm.Min_Forw.ToString());
                        template = template.Replace("@MAX_FORW@", item.classItm.Max_Forw.ToString());
                        template = template.Replace("@MAX_BACKW@", item.classItm.Max_Backw.ToString());

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    if (classRelList.Any())
                    {
                        listString = listString.Replace("@CLASSREL_LIST@", "new List<clsClassRel> { " + string.Join(", ", classRelList) + " }");
                    }
                    else
                    {
                        listString = listString.Replace("@CLASSREL_LIST@", "new List<clsClassRel>()");
                    }



                    namesToAdd.Clear();


                    var objAttList = new List<string>();
                    var objAttribs = from objAtt in enrichResult.Result.SelectMany(onto => onto.ObjectAttributes)
                                     join obj in objects on objAtt.ID_Object equals obj.GUID
                                     join attType in attributeTypes on objAtt.ID_AttributeType equals attType.GUID
                                     select new { objAtt, obj, attType };
                    request.MessageOutput?.OutputInfo($"Add ObjAtts (bool) to Assembly");
                    foreach (var item in objAttribs.Where(objAtt => objAtt.objAtt.ID_DataType == Globals.DType_Bool.GUID))
                    {
                        var objectName = GetPropertyName(item.obj.Name, regex);
                        var attributeTypeName = GetPropertyName(item.attType.Name, regex);

                        var name = $"{objectName}_{attributeTypeName}";
                        var newName = GetUniqueName(name, "ObjAtt", namesToAdd, null);

                        namesToAdd.Add(newName);
                        objAttList.Add(newName);

                        var template = Templates.ObjAttBoolProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_ATTRIBUTETYPE@", item.objAtt.ID_AttributeType);
                        template = template.Replace("@ID_ATTRIBUTE@", item.objAtt.ID_Attribute);
                        template = template.Replace("@ID_OBJECT@", item.objAtt.ID_Object);
                        template = template.Replace("@ID_CLASS@", item.objAtt.ID_Class);
                        template = template.Replace("@ID_DATATYPE@", item.objAtt.ID_DataType);
                        template = template.Replace("@ORDERID@", item.objAtt.OrderID.ToString());
                        template = template.Replace("@VAL_NAMED@", item.objAtt.Val_Named.Replace("\\", "\\\\").Replace("\"", "\\\""));
                        template = template.Replace("@VAL_BOOL@", item.objAtt.Val_Bool != null && item.objAtt.Val_Bool.Value ? "true" : "false");

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    

                    namesToAdd.Clear();

                    request.MessageOutput?.OutputInfo($"Add ObjAtts (datetime) to Assembly");
                    foreach (var item in objAttribs.Where(objAtt => objAtt.attType.ID_DataType == Globals.DType_DateTime.GUID))
                    {
                        var objectName = GetPropertyName(item.obj.Name, regex);
                        var attributeTypeName = GetPropertyName(item.attType.Name, regex);

                        var name = $"{objectName}_{attributeTypeName}";
                        var newName = GetUniqueName(name, "ObjAtt", namesToAdd, null);

                        namesToAdd.Add(newName);
                        objAttList.Add(newName);

                        var template = Templates.ObjAttDateTimeProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_ATTRIBUTETYPE@", item.objAtt.ID_AttributeType);
                        template = template.Replace("@ID_ATTRIBUTE@", item.objAtt.ID_Attribute);
                        template = template.Replace("@ID_OBJECT@", item.objAtt.ID_Object);
                        template = template.Replace("@ID_CLASS@", item.objAtt.ID_Class);
                        template = template.Replace("@ID_DATATYPE@", item.objAtt.ID_DataType);
                        template = template.Replace("@ORDERID@", item.objAtt.OrderID.ToString());
                        template = template.Replace("@VAL_NAMED@", item.objAtt.Val_Named.Replace("\\", "\\\\").Replace("\"", "\\\""));
                        template = template.Replace("@DATETIME@", item.objAtt.Val_Datetime.ToString());

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                   

                    namesToAdd.Clear();
                    request.MessageOutput?.OutputInfo($"Add ObjAtts (long) to Assembly");
                    foreach (var item in objAttribs.Where(objAtt => objAtt.objAtt.ID_DataType == Globals.DType_Int.GUID))
                    {
                        var objectName = GetPropertyName(item.obj.Name, regex);
                        var attributeTypeName = GetPropertyName(item.attType.Name, regex);

                        var name = $"{objectName}_{attributeTypeName}";
                        var newName = GetUniqueName(name, "ObjAtt", namesToAdd, null);

                        namesToAdd.Add(newName);
                        objAttList.Add(newName);

                        var template = Templates.ObjAttLongProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_ATTRIBUTETYPE@", item.objAtt.ID_AttributeType);
                        template = template.Replace("@ID_ATTRIBUTE@", item.objAtt.ID_Attribute);
                        template = template.Replace("@ID_OBJECT@", item.objAtt.ID_Object);
                        template = template.Replace("@ID_CLASS@", item.objAtt.ID_Class);
                        template = template.Replace("@ID_DATATYPE@", item.objAtt.ID_DataType);
                        template = template.Replace("@ORDERID@", item.objAtt.OrderID.ToString());
                        template = template.Replace("@VAL_NAMED@", item.objAtt.Val_Named.Replace("\\", "\\\\").Replace("\"", "\\\""));
                        template = template.Replace("@VAL_LNG@", item.objAtt.Val_Lng.ToString());

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                   

                    namesToAdd.Clear();
                    request.MessageOutput?.OutputInfo($"Add ObjAtts (real) to Assembly");
                    foreach (var item in objAttribs.Where(objAtt => objAtt.objAtt.ID_DataType == Globals.DType_Real.GUID))
                    {
                        var objectName = GetPropertyName(item.obj.Name, regex);
                        var attributeTypeName = GetPropertyName(item.attType.Name, regex);

                        var name = $"{objectName}_{attributeTypeName}";
                        var newName = GetUniqueName(name, "ObjAtt", namesToAdd, null);

                        namesToAdd.Add(newName);
                        objAttList.Add(newName);

                        var template = Templates.ObjAttDoubleProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_ATTRIBUTETYPE@", item.objAtt.ID_AttributeType);
                        template = template.Replace("@ID_ATTRIBUTE@", item.objAtt.ID_Attribute);
                        template = template.Replace("@ID_OBJECT@", item.objAtt.ID_Object);
                        template = template.Replace("@ID_CLASS@", item.objAtt.ID_Class);
                        template = template.Replace("@ID_DATATYPE@", item.objAtt.ID_DataType);
                        template = template.Replace("@ORDERID@", item.objAtt.OrderID.ToString());
                        template = template.Replace("@VAL_NAMED@", item.objAtt.Val_Named.Replace("\\", "\\\\").Replace("\"", "\\\""));
                        template = template.Replace("@VAL_DBL@", item.objAtt.Val_Double.Value.ToString(CultureInfo.InvariantCulture));

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    namesToAdd.Clear();
                    request.MessageOutput?.OutputInfo($"Add ObjAtts (string) to Assembly");
                    foreach (var item in objAttribs.Where(objAtt => objAtt.objAtt.ID_DataType == Globals.DType_String.GUID))
                    {
                        var objectName = GetPropertyName(item.obj.Name, regex);
                        var attributeTypeName = GetPropertyName(item.attType.Name, regex);

                        var name = $"{objectName}_{attributeTypeName}";
                        var newName = GetUniqueName(name, "ObjAtt", namesToAdd, null);

                        namesToAdd.Add(newName);
                        objAttList.Add(newName);

                        var template = Templates.ObjAttStringProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_ATTRIBUTETYPE@", item.objAtt.ID_AttributeType);
                        template = template.Replace("@ID_ATTRIBUTE@", item.objAtt.ID_Attribute);
                        template = template.Replace("@ID_OBJECT@", item.objAtt.ID_Object);
                        template = template.Replace("@ID_CLASS@", item.objAtt.ID_Class);
                        template = template.Replace("@ID_DATATYPE@", item.objAtt.ID_DataType);
                        template = template.Replace("@ORDERID@", item.objAtt.OrderID.ToString());
                        template = template.Replace("@VAL_NAMED@", item.objAtt.Val_Named.Replace("\"", "\"\""));
                        template = template.Replace("@VAL_STRING@", item.objAtt.Val_String.Replace("\"", "\"\""));

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    if (objAttList.Any())
                    {
                        listString = listString.Replace("@OBJATT_LIST@", "new List<clsObjectAtt> { " + string.Join(", ", objAttList) + " }");
                    }
                    else
                    {
                        listString = listString.Replace("@OBJATT_LIST@", "new List<clsObjectAtt>()");
                    }

                    namesToAdd.Clear();

                    var objRelList = new List<string>();
                    var ontologyRelationtypes = new List<clsOntologyItem>();
                    ontologyRelationtypes.Add(Globals.RelationType_belonging);
                    ontologyRelationtypes.Add(Globals.RelationType_contains);
                    ontologyRelationtypes.Add(Globals.RelationType_belongingAttribute);
                    ontologyRelationtypes.Add(Globals.RelationType_belongingClass);
                    ontologyRelationtypes.Add(Globals.RelationType_belongingRelationType);
                    ontologyRelationtypes.Add(Globals.RelationType_belongingObject);

                    relationTypes.AddRange(from rel in ontologyRelationtypes
                                           join exist in relationTypes on rel.GUID equals exist.GUID into exists
                                           from exist in exists.DefaultIfEmpty()
                                           where exist == null
                                           select rel);

                    request.MessageOutput?.OutputInfo($"Add ObjRels (full) to Assembly");
                    var relsFull = (from item in enrichResult.Result.SelectMany(onto => onto.ObjectRelations).Where(rel => !string.IsNullOrEmpty(rel.ID_Parent_Other))
                                 join obj in objects.Where(objItm => !string.IsNullOrEmpty(objItm.Name)) on item.ID_Object equals obj.GUID
                                 join clsObj in classes on item.ID_Parent_Object equals clsObj.GUID
                                 join oth in objects.Where(objItm => !string.IsNullOrEmpty(objItm.Name)) on item.ID_Other equals oth.GUID
                                 join clsOth in classes on item.ID_Parent_Object equals clsOth.GUID
                                 join relType in relationTypes on item.ID_RelationType equals relType.GUID
                                 select new { objRel = item, obj, clsObj, oth, clsOth, relType }).ToList();
                    foreach (var item in relsFull)
                    {
                        var objectName = GetPropertyName(item.obj.Name, regex);
                        var otherName = GetPropertyName(item.oth.Name, regex);
                        var relationTypeName = GetPropertyName(item.relType.Name, regex);

                        var name = $"{objectName}_{relationTypeName}_{otherName}";
                        var newName = GetUniqueName(name, "ObjRelFull", namesToAdd, null);

                        namesToAdd.Add(newName);
                        objRelList.Add(newName);

                        var template = Templates.ObjRelFullProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_OBJECT@", item.objRel.ID_Object);
                        template = template.Replace("@ID_OTHER@", item.objRel.ID_Other);
                        template = template.Replace("@ID_PARENT_OBJECT@", item.objRel.ID_Parent_Object);
                        template = template.Replace("@ID_PARENT_OTHER@", item.objRel.ID_Parent_Other);
                        template = template.Replace("@ID_RELATIONTYPE@", item.objRel.ID_RelationType);
                        template = template.Replace("@ORDERID@", item.objRel.OrderID.ToString());
                        template = template.Replace("@ONTOLOGY@", item.objRel.Ontology);

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    var others = classes.ToList();
                    others.AddRange(attributeTypes);
                    others.AddRange(relationTypes);
                    request.MessageOutput?.OutputInfo($"Add ObjRels (light) to Assembly");
                    var leftObjects = (from item in enrichResult.Result.SelectMany(onto => onto.ObjectRelations).Where(rel => rel.Ontology != Globals.Type_Object)
                                       join obj in objects.Where(objItm => !string.IsNullOrEmpty(objItm.Name)) on item.ID_Object equals obj.GUID
                                       join clsObj in classes on item.ID_Parent_Object equals clsObj.GUID
                                       select new { item, obj, clsObj }).ToList();
                    var objRelPart = (from objRel in leftObjects
                                     join oth in others on objRel.item.ID_Other equals oth.GUID
                                     join relType in relationTypes on objRel.item.ID_RelationType equals relType.GUID
                                     select new { objRel = objRel.item, objRel.obj, objRel.clsObj, oth, relType }).ToList();
                    namesToAdd.Clear();
                    foreach (var item in objRelPart)
                    {
                        var objectName = GetPropertyName(item.obj.Name, regex);
                        var otherName = GetPropertyName(item.oth.Name, regex);
                        var relationTypeName = GetPropertyName(item.relType.Name, regex);

                        var name = $"{objectName}_{relationTypeName}_{otherName}";
                        var newName = GetUniqueName(name, "ObjRel", namesToAdd, null);

                        namesToAdd.Add(newName);
                        objRelList.Add(newName);

                        var template = Templates.ObjRelPartProperty;
                        template = template.Replace("@NAME@", newName);
                        template = template.Replace("@ID_OBJECT@", item.objRel.ID_Object);
                        template = template.Replace("@ID_OTHER@", item.objRel.ID_Other);
                        template = template.Replace("@ID_PARENT_OBJECT@", item.objRel.ID_Parent_Object);
                        template = template.Replace("@ID_RELATIONTYPE@", item.objRel.ID_RelationType);
                        template = template.Replace("@ORDERID@", item.objRel.OrderID.ToString());
                        template = template.Replace("@ONTOLOGY@", item.objRel.Ontology);

                        sbOntologyAssemblyProperties.AppendLine(template);
                    }

                    if (objRelList.Any())
                    {
                        listString = listString.Replace("@OBJREL_LIST@", "new List<clsObjectRel> { " + string.Join(", ", objRelList) + " }");
                    }
                    else
                    {
                        listString = listString.Replace("@OBJREL_LIST@", "new List<clsObjectRel>()");
                    }


                    request.MessageOutput?.OutputInfo($"Create assembly...");
                    sbOntologyAssembly.Replace("@PROPERTY_LIST@", sbOntologyAssemblyProperties.ToString());
                    sbOntologyAssembly.Replace("@LIST_PROPERTY@", listString);
                    CompilerResults r = CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(parameters, sbOntologyAssembly.ToString());

                    if (r.Errors.HasErrors)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = string.Join("\r\n", r.Errors.Cast<CompilerError>().Select(err => err.ErrorText));
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo($"Created assembly.");


                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Generate: {ex.Message}";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }



                return result;
            });

            return taskResult;
        }

        public GenerateOntologyAssemblyController(Globals globals) : base(globals)
        {

            ReplaceTokens.Add(">", "gt");
            ReplaceTokens.Add("<", "lt");
            ReplaceTokens.Add("/", "slash");
        }
    }
}
